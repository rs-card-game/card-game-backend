package com.example.card.service;

import com.example.card.application.exception.TechnicalException;
import com.example.card.application.service.CardServiceImpl;
import com.example.card.domain.Card;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CardServiceImplTest {

    private CardServiceImpl cardService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        cardService = new CardServiceImpl();
    }

    @Test
    void generateHand_ValidHandSize_ReturnsListOfCards() throws TechnicalException {
        List<Card> hand = cardService.generateHand(5);
        assertEquals(5, hand.size());
    }

    @Test
    void generateHand_InvalidHandSize_ThrowsException() {
        assertThrows(TechnicalException.class, () -> cardService.generateHand(-1));
    }

}

package com.example.card.service;

import com.example.card.application.exception.TechnicalException;
import com.example.card.application.service.CardServiceImpl;
import com.example.card.application.service.SortServiceImpl;
import com.example.card.exposition.web.dto.CardDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SortServiceImplTest {


    @InjectMocks
    private SortServiceImpl sortService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void sortHand_SuccessfulSorting_ReturnsSortedHand() throws TechnicalException {
        // Mock data
        List<CardDTO> hand = new ArrayList<>();
        hand.add(new CardDTO("Coeur", "10"));
        hand.add(new CardDTO("Coeur", "Roi"));
        hand.add(new CardDTO("Pique", "5"));
        hand.add(new CardDTO("Carreaux", "10"));
        hand.add(new CardDTO("Carreaux", "Valet"));
        hand.add(new CardDTO("Trèfle", "3"));
        hand.add(new CardDTO("Pique", "9"));
        hand.add(new CardDTO("Trèfle", "3"));
        hand.add(new CardDTO("Coeur", "Roi"));
        hand.add(new CardDTO("Coeur", "Roi"));

        // Mocking ordered colors and values
        CardServiceImpl.orderedColors = List.of("Trèfle", "Coeur", "Carreaux","Pique");

        CardServiceImpl.orderedValues = List.of("3", "Roi", "Valet", "10", "As", "7", "4", "5", "9", "6", "2", "Dame", "8");

        // Test
        List<CardDTO> sortedHand = sortService.sortHand(hand);

        // Verification
        assertEquals(10, sortedHand.size());

        // Ensure the cards are sorted according to the specified order
        assertTrue(sortedHand.get(0).color().equals("Trèfle") && sortedHand.get(0).value().equals("3"));
        assertTrue(sortedHand.get(1).color().equals("Trèfle") && sortedHand.get(1).value().equals("3"));
        assertTrue(sortedHand.get(2).color().equals("Coeur") && sortedHand.get(2).value().equals("Roi"));

    }

}
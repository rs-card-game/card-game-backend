package com.example.card.controller;

import com.example.card.application.service.CardService;
import com.example.card.application.service.SortService;
import com.example.card.domain.Card;
import com.example.card.exposition.web.controller.CardController;
import com.example.card.exposition.web.dto.CardDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CardController.class)
@AutoConfigureMockMvc
class CardControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CardService cardService;
    @MockBean
    private SortService sortService;
    @Test
    void generateHand_ReturnsGeneratedHandDTO() throws Exception {
        // Mock data
        List<Card> cards = new ArrayList<>();
        cards.add(new Card("Coeur", "As"));
        cards.add(new Card("Trèfle", "2"));
        cards.add(new Card("Pique", "Roi"));


        // Mocking cardService.generateHand()
        when(cardService.generateHand(10)).thenReturn(cards);

        // Perform the request and verify the response
        mockMvc.perform(MockMvcRequestBuilders.get("/hand")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.hand").isArray())
                .andExpect(jsonPath("$.hand.length()").value(cards.size()));

        // Verify that cardService.generateHand() was called once
        verify(cardService, times(1)).generateHand(10);
    }


    @Test
    void sortHand_ReturnsSortedHand() throws Exception {
        // Mock data
        List<CardDTO> hand = new ArrayList<>();
        hand.add(new CardDTO("Coeur", "As"));
        hand.add(new CardDTO("Trèfle", "2"));
        hand.add(new CardDTO("Pique", "Roi"));
        List<CardDTO> sortedHand = new ArrayList<>(hand);
        sortedHand.sort((c1, c2) -> {
            if (c1.color().equals(c2.color())) {
                return c1.value().compareTo(c2.value());
            }
            return c1.color().compareTo(c2.color());
        });

        // Mocking sortService.sortHand()
        when(sortService.sortHand(hand)).thenReturn(sortedHand);

        // Perform the request and verify the response
        mockMvc.perform(MockMvcRequestBuilders.put("/hand/sort")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(hand)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.length()").value(sortedHand.size()));

        // Verify that sortService.sortHand() was called once with the given hand
        verify(sortService, times(1)).sortHand(hand);
    }

    // Helper method to convert objects to JSON string
    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

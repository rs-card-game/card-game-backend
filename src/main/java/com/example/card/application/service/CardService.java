package com.example.card.application.service;

import com.example.card.application.exception.TechnicalException;
import com.example.card.domain.Card;

import java.util.List;

public interface CardService {
    List<Card> generateHand(int handSize) throws TechnicalException;
}
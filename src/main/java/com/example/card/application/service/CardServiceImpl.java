package com.example.card.application.service;

import com.example.card.application.exception.TechnicalException;
import com.example.card.application.util.RandomUtils;
import com.example.card.domain.Card;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class CardServiceImpl implements CardService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CardServiceImpl.class);

    private static final String[] COLORS = {"Carreaux", "Coeur", "Pique", "Trèfle"};
    private static final String[] VALUES = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Valet", "Dame", "Roi"};

    public static List<String> orderedColors;
    public static List<String> orderedValues;

    @Override
    public List<Card> generateHand(int handSize) throws TechnicalException {
            return generateRandomHand(handSize);
    }

    private List<Card> generateRandomHand(int numberOfCards) throws TechnicalException {
        if (numberOfCards <= 0) {
            throw new TechnicalException("Number of cards must be positive.");
        }

        List<Card> hand = new ArrayList<>();
        try {
            while (hand.size() < numberOfCards) {
                String color = COLORS[RandomUtils.nextInt(COLORS.length)];
                String value = VALUES[RandomUtils.nextInt(VALUES.length)];
                Card newCard = new Card(color, value);
                if (!hand.contains(newCard)) {
                    hand.add(newCard);
                }
            }

            orderedColors = new ArrayList<>(Arrays.asList(COLORS));
            orderedValues = new ArrayList<>(Arrays.asList(VALUES));

            // shuffle lists
            Collections.shuffle(orderedColors);
            Collections.shuffle(orderedValues);
            LOGGER.info("orderedColors {}", orderedColors);
            LOGGER.info("orderedValues {}", orderedValues);
            LOGGER.info("generated hand {}", hand);
            return hand;
        } catch (ArrayIndexOutOfBoundsException e) {
            LOGGER.error("Error while generating random hand: {}", e.getMessage());
            throw new TechnicalException("Failed to generate hand due to unexpected error.");
        }

    }

}
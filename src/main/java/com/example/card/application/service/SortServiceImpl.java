package com.example.card.application.service;

import com.example.card.application.adapter.CardAdapter;
import com.example.card.application.exception.TechnicalException;
import com.example.card.domain.Card;
import com.example.card.exposition.web.dto.CardDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SortServiceImpl implements SortService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SortServiceImpl.class);

    @Override
    public List<CardDTO> sortHand(List<CardDTO> hand) throws TechnicalException {
        try {
            List<Card> cards = hand.stream().map(card -> CardAdapter.mapToCard(card)).collect(Collectors.toList());

            cards = cards.stream().sorted(comparator()).collect(Collectors.toList());
            LOGGER.info("ordered cards {}", cards);
            return cards.stream().map(card -> CardAdapter.mapToCardDTO(card)).toList();
        } catch (Exception e) {
            LOGGER.error("An error occurred while sorting hand: {}", e.getMessage());
            // You can handle the exception here, such as returning an empty list or rethrowing it
            throw new TechnicalException("An error occurred while sorting hand");
        }
    }

    private Comparator<Card> comparator() {
        Comparator<Card> colorComparator = Comparator.comparingInt(card -> CardServiceImpl.orderedColors.indexOf(card.color()));
        Comparator<Card> valuesComparator = Comparator.comparingInt(card -> CardServiceImpl.orderedValues.indexOf(card.value()));
        return colorComparator.thenComparing(valuesComparator);
    }
}

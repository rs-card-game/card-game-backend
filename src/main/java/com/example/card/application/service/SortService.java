package com.example.card.application.service;

import com.example.card.application.exception.TechnicalException;
import com.example.card.exposition.web.dto.CardDTO;

import java.util.List;

public interface SortService {
//    List<CardDTO> sortHand(List<CardDTO> hand, String sortBy);
     List<CardDTO> sortHand(List<CardDTO> hand) throws TechnicalException;
}
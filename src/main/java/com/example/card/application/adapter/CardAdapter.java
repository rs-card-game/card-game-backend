package com.example.card.application.adapter;

import com.example.card.domain.Card;
import com.example.card.exposition.web.dto.CardDTO;

public class CardAdapter {

    private CardAdapter() {
    }

    /**
     * Maps a CardDTO object to a Card object.
     */
    public static Card mapToCard(CardDTO cardDTO) {
        if (cardDTO == null) {
            return null;
        }
        return new Card(cardDTO.color(), cardDTO.value());
    }

    /**
     * Maps a Card object to a CardDTO object.
     */
    public static CardDTO mapToCardDTO(Card card) {
        if (card == null) {
            return null;
        }
        return new CardDTO(card.color(), card.value());
    }
}

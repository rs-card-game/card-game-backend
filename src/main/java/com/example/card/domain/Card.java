package com.example.card.domain;

public record Card(String color, String value) {}

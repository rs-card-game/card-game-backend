package com.example.card.exposition.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

public record GeneratedHandDTO(@Schema(description = "the hand cards")
                               List<CardDTO> hand,
                               @Schema(description = "the hand orderedColors")
                               List<String> orderedColors,
                               @Schema(description = "the hand orderedValues")
                               List<String> orderedValues) {
}

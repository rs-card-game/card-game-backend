package com.example.card.exposition.web.controller;

import com.example.card.application.adapter.CardAdapter;
import com.example.card.application.exception.TechnicalException;
import com.example.card.application.service.CardService;
import com.example.card.application.service.CardServiceImpl;
import com.example.card.application.service.SortService;
import com.example.card.exposition.web.dto.CardDTO;
import com.example.card.exposition.web.dto.GeneratedHandDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/hand")
public class CardController {


    private final CardService cardService;
    private final SortService sortService;
    @Autowired
    public CardController(CardService cardService, SortService sortService) {
        this.cardService = cardService;
        this.sortService = sortService;
    }

    @Operation(summary = "Generate cards hand", description = "Get 10 random cards with the orderedColors and the orderedValues lists")
    @ApiResponse(responseCode = "200", description = "cards hand generated")
    @GetMapping
    public GeneratedHandDTO generateHand() throws TechnicalException {
        List<CardDTO> cards = cardService.generateHand(10).stream()
                .map(CardAdapter::mapToCardDTO)
                .collect(Collectors.toList());
        return new GeneratedHandDTO(cards, CardServiceImpl.orderedColors, CardServiceImpl.orderedValues);
    }

    @Operation(summary = "sort cards hand", description = "sort hand cards by color then by value")
    @ApiResponse(responseCode = "200", description = "cards hand sorted")
    @PutMapping("/sort")
    public List<CardDTO> sortHand(@RequestBody List<CardDTO> hand) throws TechnicalException {
        List<CardDTO> sortedHand = sortService.sortHand(hand);
        return sortedHand;
    }

}
package com.example.card.exposition.web.dto;

import io.swagger.v3.oas.annotations.media.Schema;

public record CardDTO(
        @Schema(description = "the card color")
        String color,
        @Schema(description = "the card value")
        String value) {
}
